package payjs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
/**
 * 
 *@author lianlianyi@vip.qq.com
* @date 2018年1月20日 下午4:32:59
 */
public class PayJSConfig {
	/**商户号*/
	private String mchid;
	/**密钥*/
	private String key;
}