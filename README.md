#官网文档地址: https://help.payjs.cn/test/java-ban-ben.html

# live-payjs-sdk

项目介绍：
-------------------------------------
### 官网: https://payjs.cn <br> 
### 可以体验一下支付流程: http://qiongbi.chenwenxi.cc <br>
本项目是对payjs的api进行的一层封装,用于个人微信支付

Quick Start
-------------------------------------

### API 能力
- 扫码支付接口
- 收银台支付接口(JSAPI模式)
- 异步通知接口
- 订单查询接口

### 如何使用

```java
//密钥去官网申请
private WxPayApi api = new WxPayApi(new PayJSConfig("商户号", "密钥"));

/**生成支付的二维码*/
@Action
public JsonData getQr() throws IOException {
    //回调的url
    String url_huidiao = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/" + "pay/psyJs.huidiao.html";
    //封装支付信息
    ClientPayInfo payInfo = new ClientPayInfo();
    String id = "qb" + System.currentTimeMillis();
    payInfo.setTotal_fee(Convert.toLongValue(user.getAmount() * 100)).setBody(user.getEmail()).setNotify_url(url_huidiao).setOut_trade_no(id).setAttach("qb");

    ScanResult scan = api.scan(payInfo);
    user.setCreateTime(new JDateTime().getTimeInMillis());
    return new JsonData(scan);
}

/**验证订单信息(是否用户已经扫描支付完成)*/
@Action
public JsonData checkOrder() throws UnsupportedEncodingException {
    Order order = api.getOrder(orderId);//根据订单id获取订单信息
    if (order.getStatus() == 1) {// 支付成功
        this.look("支付完成");
    }
    return new JsonData(order);
}

/**收银台*/
@Action
public JsonData getWxPay() throws UnsupportedEncodingException{
    String url_huidiao = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/" + "pay/psyJs.huidiao.html";
    String callback_url = "http://qiongbi.chenwenxi.cc";//用户支付成功后，前端跳转地址
    ClientPayInfo payInfo = new ClientPayInfo();
    payInfo.setCallback_url(callback_url);
    String id = "qb" + System.currentTimeMillis();
    payInfo.setTotal_fee(Convert.toLongValue(user.getAmount() * 100)).setBody(user.getEmail()).setNotify_url(url_huidiao).setOut_trade_no(id).setAttach("qb");
    return new JsonData(payInfo);
}
```